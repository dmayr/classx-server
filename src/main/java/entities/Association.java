package entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mario on 28.01.2017.
 */
@XmlRootElement
@Entity
@NamedQueries({
        @NamedQuery(name = "Association.findAll", query = "select a from Association a")
})
@XmlAccessorType(XmlAccessType.FIELD)
public class Association {

    @Id
    @GeneratedValue
    private Long id;

    private String title;


    @OneToMany(mappedBy = "association", cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    private List<Operator> operators;


    @OneToMany(mappedBy = "association", cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    private List<Appointment> appointments;

    @XmlTransient
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Admin admin;

    public Association() {
        operators = new LinkedList<>();
        appointments = new LinkedList<>();
    }

    public Association(String title, Admin admin) {
        this.title = title;
        this.admin = admin;

        operators = new LinkedList<>();
        appointments = new LinkedList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Operator> getOperators() {
        return operators;
    }

    public void setOperators(List<Operator> operators) {
        this.operators = operators;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {

        this.admin = admin;
        admin.addAssociation(this);
    }

    public void addAppointment(Appointment appointment) {
        appointments.add(appointment);
        if(appointment.getAssociation() != this)
            appointment.setAssociation(this);
    }

    public void addOperator(Operator operator) {
        operators.add(operator);
        if(operator.getAssociation() != this)
            operator.setAssociation(this);
    }
}
