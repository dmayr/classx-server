package entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mario on 28.01.2017.
 */
@Entity
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Operator.findAll", query = "select o from Operator o"),
        @NamedQuery(name = "Operator.findByEmail", query = "select o from Operator o where lower(o.email) = lower(:EMAIL)")
})
public class Operator extends Person {

    @OneToMany(mappedBy = "operator", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private List<PersonalAppointment> personalAppointments;

    @XmlTransient
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    private Association association;

    public Operator() {
        personalAppointments = new LinkedList<>();
    }

    public Operator(String firstName, String lastName, String email, String password, Association association) {
        super(firstName, lastName, email, password);
        this.association = association;
        personalAppointments = new LinkedList<>();
    }

    public List<PersonalAppointment> getPersonalAppointments() {
        return personalAppointments;
    }

    public void setPersonalAppointments(List<PersonalAppointment> personaAppointments) {
        this.personalAppointments = personaAppointments;
    }

    public Association getAssociation() {
        return association;
    }

    public void setAssociation(Association association) {
        this.association = association;
        association.addOperator(this);
    }

    public void addPersonalAppointment(PersonalAppointment personalAppointment) {
        personalAppointments.add(personalAppointment);
        if(personalAppointment.getOperator() != this)
            personalAppointment.setOperator(this);
    }
}
