package entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mario on 28.01.2017.
 */
@XmlRootElement
@Entity
@XmlAccessorType(XmlAccessType.FIELD)
public class Admin extends Person{



    @OneToMany(mappedBy = "admin", fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private List<Association> associations;

    public Admin() {
        associations = new LinkedList<>();
    }

    public Admin(String firstName, String lastName, String email, String password) {
        super(firstName, lastName, email, password);
        associations = new LinkedList<>();
    }

    public List<Association> getAssociations() {
        return associations;
    }

    public void setAssociations(List<Association> associations) {
        this.associations = associations;
    }

    public void addAssociation(Association association) {

        associations.add(association);
        if(association.getAdmin() != this)
            association.setAdmin(this);
    }
}
