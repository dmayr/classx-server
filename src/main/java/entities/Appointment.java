package entities;

import entities.enumerations.AppointmentType;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mario on 28.01.2017.
 */
@XmlRootElement
@Entity
@NamedQueries({
        @NamedQuery(name = "Appointment.findAll", query = "select a from Appointment a")
})
@XmlAccessorType(XmlAccessType.FIELD)
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    private String description;

    @Enumerated(EnumType.STRING)
    private AppointmentType type;
    private LocalDate appointmentDate;


    @XmlTransient
    @OneToMany(mappedBy = "appointment", cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.DETACH}, fetch = FetchType.EAGER)
    private List<PersonalAppointment> personalAppointments;

    @XmlTransient
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Association association;

    public Appointment() {
        personalAppointments = new LinkedList<>();
    }

    public Appointment(String title, String description, AppointmentType type, LocalDate appointmentDate, Association association) {
        this();
        this.title = title;
        this.description = description;
        this.type = type;
        this.appointmentDate = appointmentDate;
        this.association = association;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AppointmentType getType() {
        return type;
    }

    public void setType(AppointmentType type) {
        this.type = type;
    }

    public LocalDate getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(LocalDate appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public List<PersonalAppointment> getOperators() {
        return personalAppointments;
    }

    public void setOperators(List<PersonalAppointment> personalAppointments) {
        this.personalAppointments = personalAppointments;
    }

    public Association getAssociation() {
        return association;
    }

    public void setAssociation(Association association) {

        this.association = association;
        association.addAppointment(this);
    }

    public void addPersonalAppointment(PersonalAppointment personalAppointment) {
        personalAppointments.add(personalAppointment);
        if(personalAppointment.getAppointment() != this)
            personalAppointment.setAppointment(this);
    }
}
