package entities.enumerations;

/**
 * Created by Mario on 28.01.2017.
 */
public enum AppointmentType {
    TEST, HOMEWORK, LZK
}
