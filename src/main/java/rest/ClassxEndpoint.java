package rest;

import business.AssociationFacade;
import business.service.CryptoController;
import business.service.EMail;
import business.OperatorFacade;
import entities.Association;
import entities.Operator;
import entities.Person;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

/**
 * Created by Mario on 06.02.2017.
 */
@Path("classx")
public class ClassxEndpoint {

    @Inject
    OperatorFacade operatorFacade;

    @Inject
    AssociationFacade associationFacade;

    @GET
    @Path("findAllOperators")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Operator> getAllOperators() {
        return operatorFacade.findAll();
    }

    @POST
    @Path("registerOperator")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerOperator(@Context UriInfo uriInfo, Operator o) {

        if(operatorFacade.findOperatorByEmail(o.getEmail()) == null) {
            operatorFacade.create(o);
            EMail.sendRegisterMail(o.getEmail(), o.getFirstName());
            return Response.created(uriInfo.getAbsolutePathBuilder().path("/" + o.getId()).build()).build();
        }
        return Response.notModified("Operator: " + o + " already exist in DB").build();
    }

    @POST
    @Path("login")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response checkLoginPerson(Person person) {
        Person exists = operatorFacade.findOperatorByEmail(person.getEmail());
        if(exists == null)
            return Response.notModified("Operator with e-mail: " + person.getEmail() + " doesn't exist").build();

        String unlockedPassword = "";

        try{
            CryptoController cc = new CryptoController(person.getPassword());
            unlockedPassword = cc.decrypt(exists.getPassword());
        }catch (Exception e) {
            cryptoException("decrypt");
        }

        if(unlockedPassword.equals(person.getPassword()))
            return Response.ok(person.getUserName() + " logged in successfully").build();
        else
            return Response.notModified("Wrong password, please try again").build();
    }

    @PUT
    @Path("updateOperator/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateOperator(@PathParam("id") long id, Operator o, @Context UriInfo uriInfo) {
        operatorFacade.update(id, o);
        return Response.created(uriInfo.getAbsolutePathBuilder().path("/" + id).build()).build();
    }

    @PUT
    @Path("joinAssociation/{associationId}/operator/{operatorId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response operatorJoinAssociation(@PathParam("associationId") long associationId, @PathParam("operatorId") long operatorId) {
        Association association = associationFacade.findById(associationId);
        Operator operator = operatorFacade.findById(operatorId);
        if(association == null)
            return Response.notModified("Association doesn't exist").build();
        if(operator == null)
            return Response.notModified("Operator doesn't exits").build();

        operator.setAssociation(association);
        operatorFacade.update(operator.getId(), operator);
        return Response.ok("Operator with id: " + operator.getId() + " joined Association: " + association.getTitle()).build();
    }

    private Response cryptoException(String enOrDeCrypt) {
        return Response.notModified(enOrDeCrypt + "ion Error").build();
    }

    @GET
    @Path("findOperatorById/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Operator findOperatorById(@PathParam("id") long id) {
        return  operatorFacade.findById(id);
    }
}
