package rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by Mario on 06.02.2017.
 */
@ApplicationPath("rs")
public class ApplicationServer extends Application{
}
