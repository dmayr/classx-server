package business;

import entities.*;
import entities.enumerations.AppointmentType;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.time.LocalDate;

/**
 * Created by Mario on 29.01.2017.
 */
@Startup
@Singleton
public class InitBean {

    @Inject
    AppointmentFacade appointmentFacade;

    @Inject
    OperatorFacade operatorFacade;

    @Inject
    PersonalAppointmentFacade personalAppointmentFacade;

    @Inject
    AssociationFacade associationFacade;

    @PostConstruct
    public void init() {
        System.out.println("*** InitBean successfully started!! ***");

        initDb();

    }

    public void initDb() {
        Admin admin = new Admin("classx", "sysadmin", "sysadmin.classx@gmail.com", "scheis");

        Association class1 = new Association("4ahif", admin);
        Association class2 = new Association("4bhif", admin);

        Operator o1 = new Operator("Mario", "Lanz", "asdfadsf@gmx.at", "scheis", class1);
        Operator o2 = new Operator("Daniel", "Mayr", "asdafas@gmx.at", "scheis", class1);
        Operator o3 = new Operator("Dima", "Mühleder", "dsfasgagr@gmx.at", "scheis", class2);
        Operator o4 = new Operator("Michi", "Kotek", "masdgaasw@gmx.at", "scheis", class2);

        Appointment app1 = new Appointment("Mathe SA", "Stoff: asofhas", AppointmentType.TEST, LocalDate.now(), class1);
        Appointment app2 = new Appointment("Deutsch SA", "Stoff: asofhas", AppointmentType.TEST, LocalDate.now(), class1);
        Appointment app3 = new Appointment("Englisch SA", "Stoff: asofhas", AppointmentType.TEST, LocalDate.now(), class1);
        Appointment app4 = new Appointment("Physik Test", "Stoff: asofhas", AppointmentType.LZK, LocalDate.now(), class2);
        Appointment app5 = new Appointment("Mathe HÜ", "Stoff: asofhas", AppointmentType.HOMEWORK, LocalDate.now(), class2);
        Appointment app6 = new Appointment("Geschichte WH", "Stoff: asofhas", AppointmentType.LZK, LocalDate.now(), class2);
        Appointment app7 = new Appointment("Chemie x-Fragen", "Stoff: asofhas", AppointmentType.HOMEWORK, LocalDate.now(), class1);
        Appointment app8 = new Appointment("Chemie MAP", "Stoff: asofhas", AppointmentType.HOMEWORK, LocalDate.now(), class2);

        associationFacade.create(class1);
        associationFacade.create(class2);

        operatorFacade.create(o1);
        operatorFacade.create(o2);
        operatorFacade.create(o3);
        operatorFacade.create(o4);

        appointmentFacade.create(app1);
        appointmentFacade.create(app2);
        appointmentFacade.create(app3);
        appointmentFacade.create(app4);
        appointmentFacade.create(app5);
        appointmentFacade.create(app6);
        appointmentFacade.create(app7);
        appointmentFacade.create(app8);

        personalAppointmentFacade.create(new PersonalAppointment(o1, app1));
        personalAppointmentFacade.create(new PersonalAppointment(o1, app2));
        personalAppointmentFacade.create(new PersonalAppointment(o2, app2));
        personalAppointmentFacade.create(new PersonalAppointment(o2, app3));
        personalAppointmentFacade.create(new PersonalAppointment(o3, app3));
        personalAppointmentFacade.create(new PersonalAppointment(o3, app4));
        personalAppointmentFacade.create(new PersonalAppointment(o4, app4));
        personalAppointmentFacade.create(new PersonalAppointment(o4, app5));
        personalAppointmentFacade.create(new PersonalAppointment(o1, app5));
        personalAppointmentFacade.create(new PersonalAppointment(o1, app6));
        personalAppointmentFacade.create(new PersonalAppointment(o2, app6));
        personalAppointmentFacade.create(new PersonalAppointment(o2, app7));
        personalAppointmentFacade.create(new PersonalAppointment(o3, app7));
        personalAppointmentFacade.create(new PersonalAppointment(o3, app8));
        personalAppointmentFacade.create(new PersonalAppointment(o4, app8));
        personalAppointmentFacade.create(new PersonalAppointment(o4, app1));
    }
}
