package business.service;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

/**
 * Created by Mario on 01.02.2017.
 */
public class CryptoController {
    private SecretKeySpec key;

    public CryptoController(String inputKey) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        byte[] bytes = inputKey.getBytes("UTF-8");
        MessageDigest sha = MessageDigest.getInstance("MD5");
        bytes = sha.digest(bytes);
        bytes = Arrays.copyOf(bytes, 16);
        key = new SecretKeySpec(bytes, "AES");
    }

    public String encrypt (String text) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        // Verschluesseln
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encrypted = cipher.doFinal(text.getBytes());

        // bytes zu Base64-String konvertieren (dient der Lesbarkeit)
        return Base64.getEncoder().encodeToString(encrypted);
    }

    public String decrypt (String text) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
        // BASE64 String zu Byte-Array konvertieren
        byte[] crypted2 = Base64.getDecoder().decode(text);

        // Entschluesseln
        Cipher cipher2 = Cipher.getInstance("AES");
        cipher2.init(Cipher.DECRYPT_MODE, key);
        byte[] cipherData2 = cipher2.doFinal(crypted2);
        return new String(cipherData2);
    }
}
