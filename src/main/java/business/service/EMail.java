package business.service;


import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by Mario on 01.02.2017.
 */
public class EMail {
    public static boolean passwordRequestMail(String to, String firstname, String password){
        String subject = "Forgot Password by Classx";
        StringBuilder text = new StringBuilder();
        text.append("<h1>Hi " + firstname + ",</h1><br>");
        text.append("<p>You recently requested to get your password for your ClassX Account.</p><br>");
        text.append("<p>Your Password: " + password + "</p><br>");
        text.append("<p>Thanks,</p>");
        text.append("<p>The ClassX Team<p>");
        return sendEmail(to, subject, text.toString());
    }

    public static boolean sendRegisterMail(String to, String firstname){
        String subject = "Welcome to ClassX";
        StringBuilder text = new StringBuilder();
        text.append("<h1>Hi " + firstname + "!</h1><br>");
        text.append("<h2>Welcome to ClassX! Thanks so much for joining us. You�re on your way to super-productivity and beyond!</h2><br>");
        text.append("<p>ClassX is an app that helps you focus on the important things in life by organizing your stuff. That means it collects all your termins and remembers you when they have to be done. Set and track daily, weekly, and monthly priorities � and say goodby to chaos.</p><br>");
        text.append("<p>Have any questions? Just shoot us an email! We're always here to help.</p><br>");
        text.append("<p>Cheerfully yours,<p>");
        text.append("<p>The ClassX Team</p>");
        return sendEmail(to, subject, text.toString());
    }

    public static boolean sendEmail(String receiver, String subject, String textInHTML){
        try {
            String sender = "classx@gmx.at";
            String password = "Project1";

            final Properties properties = new Properties();

            properties.put("mail.transport.protocol", "smtp");
            properties.put("mail.smtp.host", "mail.gmx.net");
            properties.put("mail.smtp.port", "587");
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.user", sender);
            properties.put("mail.smtp.password", password);
            properties.put("mail.smtp.starttls.enable", "true");

            Session mailSession = Session.getInstance(properties, new Authenticator()
            {
                @Override
                protected PasswordAuthentication getPasswordAuthentication()
                {
                    return new PasswordAuthentication(properties.getProperty("mail.smtp.user"),
                            properties.getProperty("mail.smtp.password"));
                }
            });

            Message message = new MimeMessage(mailSession);
            InternetAddress addressTo = new InternetAddress(receiver);
            message.setRecipient(Message.RecipientType.TO, addressTo);
            message.setFrom(new InternetAddress(sender));
            message.setSubject(subject);
            message.setContent(textInHTML, "text/html");
            Transport.send(message);
        } catch (AddressException ex) {
            return false;
        } catch (MessagingException ex) {
            return false;
        }
        return true;
    }
}
