package business;

import business.service.CryptoController;
import entities.Operator;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Created by Mario on 01.02.2017.
 */
@Stateless
public class OperatorFacade {

    @PersistenceContext
    EntityManager em;

    private CryptoController cc;

    public OperatorFacade (){
    }

    public void create(Operator operator) {
        try {
            String password = operator.getPassword();
            cc = new CryptoController(password);
            operator.setPassword(cc.encrypt(password));
            operator.setUserName();
            this.em.persist(operator);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    public Operator findOperatorByEmail(String email) {
        Operator operator = null;

        try{
            operator = this.em.createNamedQuery("Operator.findByEmail", Operator.class).setParameter("EMAIL", email).getSingleResult();
        } catch(NoResultException e) {
            return null;
        }

        return operator;
    }

    public void update(long id, Operator operator) {
        operator.setId(id);
        this.em.merge(operator);
    }

    public List<Operator> findAll() {
        return this.em.createNamedQuery("Operator.findAll", Operator.class).getResultList();
    }

    public void delete(long id) {
        Operator operator = this.em.getReference(Operator.class, id);
        this.em.remove(operator);
    }

    public Operator findById(long operatorId) {
        return this.em.find(Operator.class, operatorId);
    }
}
