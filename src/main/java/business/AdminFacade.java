package business;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Mario on 01.02.2017.
 */
@Stateless
public class AdminFacade {

    @PersistenceContext
    EntityManager em;
}
