package business;

import entities.Appointment;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Mario on 01.02.2017.
 */
@Stateless
public class AppointmentFacade {

    @PersistenceContext
    EntityManager em;

    public void create(Appointment appointment) {
        this.em.persist(appointment);
    }

    public List<Appointment> findAll() {
        return this.em.createNamedQuery("Appointment.findAll", Appointment.class).getResultList();
    }

    public void delete(long id) {
        Appointment appointment = this.em.getReference(Appointment.class, id);
        this.em.remove(appointment);
    }

    public void update(long id, Appointment appointment) {
        appointment.setId(id);
        this.em.merge(appointment);
    }
}
