package business;

import entities.PersonalAppointment;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Mario on 06.02.2017.
 */
@Stateless
public class PersonalAppointmentFacade {

    @PersistenceContext
    EntityManager em;

    public PersonalAppointmentFacade(){}

    public void create(PersonalAppointment personalAppointment) {
        this.em.persist(personalAppointment);
    }
}
