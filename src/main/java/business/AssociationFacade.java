package business;

import entities.Association;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Mario on 01.02.2017.
 */
@Stateless
public class AssociationFacade {

    @PersistenceContext
    EntityManager em;

    public void create(Association association) {
        this.em.persist(association);
    }

    public List<Association> findAll() {
        return this.em.createNamedQuery("Association.findAll", Association.class).getResultList();
    }

    public void delete(long id) {
        Association association = this.em.getReference(Association.class, id);
        this.em.remove(association);
    }

    public void update(long id, Association association) {
        association.setId(id);
        this.em.merge(association);
    }

    public Association findById(long id) {
        return this.em.find(Association.class, id);
    }
}
