# Original Path
    http://vm90.htl-leonding.ac.at:8080/classx-server/rs/classx/

# Classx Server
## Rest routes
### GET
    Path:       findAllOperators
    Parameter:  none
    Return:     List<Operator>
    
    
    Path:       findOperatorById/{id}
    Parameter:  (PathParam)long
    Return:     Operator

### POST
    Path:       registerOperator 
    Parameter:  UriInfo, Operator
    Return:     Response


    Path:       login
    Parameter:  Person
    Return:     Response

### PUT
    Path:       updateOperator/{id}
    Parameter:  (PathParam)long, Operator, UriInfo
    Return:     Response
    
    
    Path:       joinAssociation/{associationId}/operator/{operatorId}
    Parameter:  (PathParam)long asId, (PathParam)long opId
    Return:     Response